package com.alsnightsoft.vaadin.testcontainer.controllers.restful

class AppController {

    def index() {
        redirect(url: '/app/')
    }
}
