import com.alsnightsoft.vaadin.testcontainer.domains.Student
import org.apache.commons.lang3.RandomStringUtils

class BootStrap {

    def init = { servletContext ->
        print "Start Inserts ... "
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join("")
        Random random = new Random()
        for (int i = 0; i < 2000; i++) {
            new Student(name: RandomStringUtils.random(10, charset.toCharArray()),
                    enabled: random.nextBoolean()).save(flush: true, failOnError: true)
        }
        print "OK"
    }
    def destroy = {
    }
}
