package com.alsnightsoft.vaadin.testcontainer.domains

class Student {

    String name
    String description

    boolean enabled = true

    static constraints = {
        description nullable: true
    }

    static mapping = {
        table "test_students"

        name sqlType: 'text'
        description sqlType: 'text'
    }

}
