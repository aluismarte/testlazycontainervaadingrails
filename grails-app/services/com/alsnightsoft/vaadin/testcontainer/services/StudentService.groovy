package com.alsnightsoft.vaadin.testcontainer.services

import com.alsnightsoft.vaadin.testcontainer.domains.Student
import grails.transaction.Transactional

@Transactional
class StudentService {

    public List<Student> listStudents(boolean enab, int start, int size) {
        return Student.withCriteria {
            firstResult(start)
            maxResults(size)
            if (enab) {
                eq "enabled", true
            }
        }.findAll()
    }

    public int countStudents(boolean enab) {
        if (enab) {
            return Student.countByEnabled(enab)
        }
        return Student.count()
    }

    public List<Student> listFilterStudents(boolean enab, String filterName, int start, int size) {
        return Student.withCriteria {
            firstResult(start)
            maxResults(size)
            ilike "name", "%" + filterName + "%"
            if (enab) {
                eq "enabled", true
            }
        }.findAll()
    }

    public int countFilterStudents(boolean enab, String filterName) {
        if (enab) {
            return Student.countByNameIlikeAndEnabled("%" + filterName + "%", enab)
        }
        return Student.countByNameIlike("%" + filterName + "%")
    }

}
