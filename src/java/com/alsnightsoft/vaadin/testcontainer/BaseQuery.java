package com.alsnightsoft.vaadin.testcontainer;

import com.vaadin.data.Item;
import org.vaadin.addons.lazyquerycontainer.Query;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by aluis on 10/8/15.
 */
public abstract class BaseQuery<BEANTYPE> implements Query {

    protected BaseQueryFactory<BEANTYPE> baseQueryFactory;
    public boolean filtered = false;

    protected BaseQuery(BaseQueryFactory<BEANTYPE> baseQueryFactory) {
        this.baseQueryFactory = baseQueryFactory;
    }

    @Override
    public int size() {
        if (filtered) {
            return baseQueryFactory.getLazyQuery().getFilteredSize();
        }
        return baseQueryFactory.getLazyQuery().getSize();
    }

    @SuppressWarnings("Convert2streamapi")
    @Override
    public List<Item> loadItems(int startIndex, int numberOfIds) {
        List<Item> items = new ArrayList<>();
        List<BEANTYPE> allData;
        if (filtered) {
            allData = baseQueryFactory.getLazyQuery().getFilteredItemsIds(startIndex, numberOfIds);
        } else {
            allData = baseQueryFactory.getLazyQuery().getItemsIds(startIndex, numberOfIds);
        }
        for (BEANTYPE data : allData) {
            items.add(baseQueryFactory.constructItem(data));
        }
        return items;
    }

    @Override
    public void saveItems(List<Item> list, List<Item> list1, List<Item> list2) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean deleteAllItems() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Item constructItem() {
        return baseQueryFactory.constructItem();
    }
}
