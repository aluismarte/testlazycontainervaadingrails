package com.alsnightsoft.vaadin.testcontainer;

import org.vaadin.addons.lazyquerycontainer.LazyQueryContainer;
import org.vaadin.addons.lazyquerycontainer.LazyQueryDefinition;
import org.vaadin.addons.lazyquerycontainer.LazyQueryView;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

/**
 *  Created by aluis on 10/8/15.
 */
public abstract class BaseContainer extends LazyQueryContainer {

    public BaseContainer(QueryFactory queryFactory) {
        super(new LazyQueryDefinition(true, BaseQueryFactory.BATCH_GRID_SIZE, BaseQueryFactory.OBJ), queryFactory);
        addContainerProperty(BaseQueryFactory.OBJ, Object.class, null, false, false);
    }

    @SuppressWarnings("unused")
    public void addDebug() {
        addContainerProperty(LazyQueryView.DEBUG_PROPERTY_ID_QUERY_INDEX, Integer.class, 0, true, false);
        addContainerProperty(LazyQueryView.DEBUG_PROPERTY_ID_BATCH_INDEX, Integer.class, 0, true, false);
        addContainerProperty(LazyQueryView.DEBUG_PROPERTY_ID_BATCH_QUERY_TIME, Long.class, 0, true, false);
        addContainerProperty(LazyQueryView.PROPERTY_ID_ITEM_STATUS, Enum.class, 0, true, false);
    }

    public void addNameColumn() {
        addContainerProperty("name", String.class, "", false, false);
    }

    public void addDescriptionColumn() {
        addContainerProperty("description", String.class, "", false, false);
    }

    public void addEnableColumn() {
        addContainerProperty("enabled", Boolean.class, true, false, false);
    }

    public abstract void addColumns();
}
