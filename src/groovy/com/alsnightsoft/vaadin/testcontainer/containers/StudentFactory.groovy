package com.alsnightsoft.vaadin.testcontainer.containers

import com.alsnightsoft.vaadin.testcontainer.BaseQueryFactory
import com.alsnightsoft.vaadin.testcontainer.LazyQuery
import com.alsnightsoft.vaadin.testcontainer.domains.Student
import com.vaadin.data.Item
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by aluis on 10/16/15.
 */
class StudentFactory extends BaseQueryFactory<Student> {

    public StudentFactory(LazyQuery<Student> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return constructItem(new Student())
    }

    @Override
    Object createProperty(Object propertyID, Student dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "id":
                return dataObject.getId()
            case "name":
                return dataObject.getName()
            case "description":
                return dataObject.getDescription()
            case "enabled":
                return dataObject.getEnabled()
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new StudentQuery(this)
    }
}
