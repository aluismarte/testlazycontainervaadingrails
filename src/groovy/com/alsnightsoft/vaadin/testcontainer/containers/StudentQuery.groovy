package com.alsnightsoft.vaadin.testcontainer.containers

import com.alsnightsoft.vaadin.testcontainer.BaseQuery
import com.alsnightsoft.vaadin.testcontainer.BaseQueryFactory
import com.alsnightsoft.vaadin.testcontainer.domains.Student

/**
 *  Created by aluis on 10/16/15.
 */
class StudentQuery extends BaseQuery<Student> {

    public StudentQuery(BaseQueryFactory baseQueryFactory) {
        super(baseQueryFactory)
    }
}
