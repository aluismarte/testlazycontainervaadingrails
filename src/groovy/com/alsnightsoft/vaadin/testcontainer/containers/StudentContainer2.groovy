package com.alsnightsoft.vaadin.testcontainer.containers

import com.alsnightsoft.vaadin.testcontainer.BaseContainer
import com.alsnightsoft.vaadin.testcontainer.LazyQuery
import com.alsnightsoft.vaadin.testcontainer.domains.Student

/**
 *  Created by aluis on 10/16/15.
 */
class StudentContainer2 extends BaseContainer {

    public StudentContainer2(LazyQuery<Student> lazyQuery) {
        super(new StudentFactory(lazyQuery))

        addColumns()
    }

    @Override
    public void addColumns() {
        addContainerProperty("id", Long.class, 0l, false, false)
        addNameColumn()
        addDescriptionColumn()
        addEnableColumn()
    }
}
