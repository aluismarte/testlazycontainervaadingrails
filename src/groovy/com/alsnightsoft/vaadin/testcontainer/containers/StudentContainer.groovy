package com.alsnightsoft.vaadin.testcontainer.containers

import com.alsnightsoft.vaadin.containers.LazyPagedContainer
import com.alsnightsoft.vaadin.testcontainer.domains.Student

/**
 *  Created by Ing. Aluis Marte on 2/1/2015.
 */
class StudentContainer extends LazyPagedContainer<Student> implements Serializable {

    StudentContainer() {
        super(Student.class)
    }
}
