package com.alsnightsoft.vaadin.testcontainer

import com.alsnightsoft.vaadin.testcontainer.containers.StudentContainer2
import com.alsnightsoft.vaadin.testcontainer.domains.Student
import com.alsnightsoft.vaadin.testcontainer.services.StudentService
import com.vaadin.event.SelectionEvent
import com.vaadin.grails.Grails
import com.vaadin.server.VaadinRequest
import com.vaadin.ui.Grid
import com.vaadin.ui.UI
import com.vaadin.ui.VerticalLayout

/**
 *  Created by Ing. Aluis Marte on 2/1/2015.
 */
class Demo2 extends UI {

    private VerticalLayout mainLayout

    private StudentContainer2 container

    private Grid tableDemo

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        mainLayout = new VerticalLayout()

        container = new StudentContainer2(new LazyQuery<Student>() {
            @Override
            int getSize() {
                return Grails.get(StudentService).countStudents(true)
            }

            @Override
            List<Student> getItemsIds(int startIndex, int numberOfIds) {
                return Grails.get(StudentService).listStudents(true, startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<Student> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return new ArrayList<Student>()
            }
        })

        tableDemo = new Grid()
        tableDemo.setSelectionMode(Grid.SelectionMode.SINGLE)
        tableDemo.setImmediate(true)
        tableDemo.setSizeFull()
        tableDemo.setFooterVisible(true)
        tableDemo.setContainerDataSource(container)
        tableDemo.setColumnOrder("id", "name", "description", "enabled")
        tableDemo.getColumn("id").setHeaderCaption("ID")
        tableDemo.getColumn("name").setHeaderCaption("NAME")
        tableDemo.getColumn("description").setHeaderCaption("DESC")
        tableDemo.getColumn("enabled").setHeaderCaption("ACTIVE")
        tableDemo.removeColumn("OBJ")
        tableDemo.addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent selectionEvent) {
                println "Student ID: " + ((Student) tableDemo.getSelectedRow()).getId()
                println "Student Name: " + ((Student) tableDemo.getSelectedRow()).getName()
            }
        })

        tableDemo.clearSortOrder()
        mainLayout.addComponent(tableDemo)

        setContent(mainLayout)
    }
}
