package com.alsnightsoft.vaadin.testcontainer

import com.alsnightsoft.vaadin.containers.LazyQuery
import com.alsnightsoft.vaadin.testcontainer.containers.StudentContainer
import com.alsnightsoft.vaadin.testcontainer.domains.Student
import com.alsnightsoft.vaadin.testcontainer.services.StudentService
import com.vaadin.data.Property
import com.vaadin.event.SelectionEvent
import com.vaadin.grails.Grails
import com.vaadin.server.VaadinRequest
import com.vaadin.ui.*

/**
 *  Created by Ing. Aluis Marte on 2/1/2015.
 */
class Demo extends UI {

    private VerticalLayout mainLayout

    private StudentContainer container

    private TextField tfFilterName
    private CheckBox chkEnable
    private Grid tableDemo

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        mainLayout = new VerticalLayout()

        container = new StudentContainer()

        tfFilterName = new TextField("Filter")
        tfFilterName.setImmediate(true)
        tfFilterName.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (tfFilterName.getValue().equals("")) {
                    container.setApplyLazyFilter(false)
                } else {
                    container.setApplyLazyFilter(true)
                }
                tableDemo.clearSortOrder()
            }
        })

        chkEnable = new CheckBox("Show Disable")
        chkEnable.setImmediate(true)
        chkEnable.setValue(false)
        chkEnable.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                tableDemo.clearSortOrder()
            }
        })

        container.setLazyQuery(new LazyQuery<Student>() {
            @Override
            int getLazySize() {
//                println "Cantidad " + Grails.get(StudentService).countStudents(!chkEnable.getValue())
                return Grails.get(StudentService).countStudents(!chkEnable.getValue())
            }

            @Override
            List<Student> getLazyItemsIds(int startIndex, int numberOfIds) {
//                println "Inicia en " + startIndex + " Pido " + numberOfIds + " Items"
                return Grails.get(StudentService).listStudents(!chkEnable.getValue(), startIndex, numberOfIds)
            }

            @Override
            int getLazyFilteredSize() {
//                println "Cantidad filtrada " + Grails.get(StudentService).countFilterStudents(tfFilterName.getValue(), tfFilterName.getValue())
                return Grails.get(StudentService).countFilterStudents(!chkEnable.getValue(), tfFilterName.getValue())
            }

            @Override
            List<Student> getLazyFilteredItemsIds(int startIndex, int numberOfIds) {
//                println "Filtro Inicia en " + startIndex + " Pido " + numberOfIds + " Items"
                return Grails.get(StudentService).listFilterStudents(!chkEnable.getValue(), tfFilterName.getValue(), startIndex, numberOfIds)
            }
        })
        // disable use filter query (default false)
        container.setApplyLazyFilter(false)

        tableDemo = new Grid()
        tableDemo.setSelectionMode(Grid.SelectionMode.SINGLE)
        tableDemo.setImmediate(true)
        tableDemo.setSizeFull()
        tableDemo.setFooterVisible(true)
        tableDemo.setContainerDataSource(container)
        tableDemo.setColumnOrder("id", "name", "enabled")
        tableDemo.getColumn("id").setHeaderCaption("ID")
        tableDemo.getColumn("name").setHeaderCaption("Name")
        tableDemo.getColumn("enabled").setHeaderCaption("Eabled")
        // ORM Details
        tableDemo.removeColumn("dirty")
        tableDemo.removeColumn("version")
        tableDemo.removeColumn("dirtyPropertyNames")
        tableDemo.removeColumn("attached")
        tableDemo.removeColumn("metaClass")
        tableDemo.removeColumn("properties")
        tableDemo.removeColumn("errors")
        tableDemo.addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent selectionEvent) {
                println "Click on a value"
                println "Student Name: " + ((Student) tableDemo.getSelectedRow()).getName()
            }
        })

        tableDemo.clearSortOrder()

        mainLayout.addComponent(tfFilterName)
        mainLayout.addComponent(chkEnable)
        mainLayout.addComponent(tableDemo)

        setContent(mainLayout)
    }
}
